#version 130
//Set this define for shadertoy
//#define shadertoy

#ifndef shadertoy
#define fragColor gl_FragColor
#define fragCoord gl_FragCoord
#define iResolution u_resolution
#define iMouse u_mouse
#define iTime u_time
#endif

#define PI 3.1415926538

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

const int MAX_STEPS = 1000;
const float MIN_RAY_LEN = 0.;
const float MAX_RAY_LEN = 10000.;
const float RAY_HIT_THRESH = 0.01;

const uint k = 1103515245U;  // GLIB C

vec3 uhash(uvec3 x) {
    x = ((x>>8U)^x.yzx)*k;
    x = ((x>>8U)^x.yzx)*k;
    x = ((x>>8U)^x.yzx)*k;
    return vec3(x)*(1.0/float(0xffffffffU));
}

vec3 hash(vec3 x) {
    return uhash(uvec3(abs(x)));
}

vec3 rotateAxis(vec3 p, vec3 axis, float angle) {
    return mix(dot(axis, p)*axis, p, cos(angle)) + cross(axis,p)*sin(angle); // ??? idk, it's from blackle
}

float sphere(vec3 samplePoint, float r) {
    return length(samplePoint) - r;
}

float orbsSDF(vec3 samplePoint) {
    return 0.;
}

float cube(vec3 samplePoint, float r) {
  vec3 q = abs(samplePoint) - r;
  return length(max(q,0.)) + min(max(q.x,max(q.y,q.z)),0.);
}

vec3 domain(vec3 samplePoint) {
    vec2 xz = floor(samplePoint.xz+0.5)-0.5;
    return vec3(xz.x, 0., xz.y);
}

vec3 posInDomain(vec3 samplePoint) {
    vec2 xz = fract(samplePoint.xz+0.5)-0.5;
    return vec3(xz.x, samplePoint.y, xz.y);
}

float worstFloorSDF(vec3 samplePoint) {
    vec2 distanceToEdges = 0.55 - abs(posInDomain(samplePoint).xz);
    return length(vec2(
                min(distanceToEdges.x, distanceToEdges.y),
                max(0., samplePoint.y - 1.)
                ));
}

float cubeAnimation(float speed) {
    float phase = fract((iTime+2.) * (0.3+0.5*speed));
    float springPortion = 0.08;
    return smoothstep(0.4, 1., 1.-min(phase/springPortion, 1.))+phase;
}
float accurateFloorSDF(vec3 samplePoint) {
    return cube(
                vec3(
                    fract(samplePoint.x+0.5)-0.5,
                    samplePoint.y-0.5+0.8*(1.-cubeAnimation(
                        hash(domain(samplePoint)).x
                        )),
                    fract(samplePoint.z+0.5)-0.5
                    ),
                0.4
                );
}

float floorSDF(vec3 samplePoint) {
    return min(samplePoint.y, min(worstFloorSDF(samplePoint), accurateFloorSDF(samplePoint)));
}

float scene(vec3 samplePoint) {
    return floorSDF(samplePoint);
}

float cheat(float y, vec3 direction) {
    float height = y - 1.;
    if (height < 0.) {
        return 0.;
    }
    float mult = height/direction.y;
    return length(vec3(height, direction.x*mult, direction.z*mult));
}
float sceneCheat(vec3 samplePoint, vec3 direction) {
    return max(
            scene(samplePoint),
            cheat(samplePoint.y, direction)
            );
}

vec3 rayDirection(vec2 fragCoord, vec2 image_size, float fov) {
    vec2 xy = fragCoord.xy - image_size.xy / 2.;
    float z = min(image_size.x, image_size.y) / tan(radians(fov) / 2.); // we don't need length(xy) here because fov just refers to the fov of a single axis
    return normalize(vec3(xy, -z));
}

float shortestDistance(vec3 cameraPosition, vec3 rayDirection) {
    float len = MIN_RAY_LEN;
    for (int i=0; i < MAX_STEPS; i++) {
        float dist = sceneCheat(cameraPosition + len * rayDirection, rayDirection);
        // isn't there a better way than just getting infinitesimally closer till we're within some tiny radius?
        // if we figured out the normal when we get close to the final surface, couldn't we extrapolate an expected point of intersection that way? then jump up to that, but allow the loop to continue in case we're just grazing past
        if (dist < RAY_HIT_THRESH) {
            return len + dist;
        }
        len += dist;
        if (len >= MAX_RAY_LEN) {
            return MAX_RAY_LEN;
        }
    }
    return MAX_RAY_LEN;
}

vec3 normal(vec3 p) {
    float original_dist = scene(p);
    // we lose a little precision by not having our normal centered on the target point but rather sticking out a bit by RAY_HIT_THRESH, but we're saving 2 SDF evaluations
    return normalize(vec3(
        scene(vec3(p.x + RAY_HIT_THRESH, p.y, p.z)) - original_dist,
        scene(vec3(p.x, p.y + RAY_HIT_THRESH, p.z)) - original_dist,
        scene(vec3(p.x, p.y, p.z + RAY_HIT_THRESH)) - original_dist
    ));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    float time = iTime*2.;
    float res = 1.;
    vec3 cameraPosition = vec3(-time, 1.+(cos(mod(time/23., 2.*PI))*0.5+0.5)*2., -time);
    fragCoord = floor(fragCoord/res)*res;
    vec3 dir = rotateAxis(
            rotateAxis(
                rayDirection(fragCoord, iResolution.xy, 140.), vec3(-1., 0., 0.), PI*((sin(mod(time/12.+PI/2., 2.*PI))*0.5+0.5)*0.3+0.2)
                ),
            vec3(0., 1., 0.), PI*0.25
            );
    float dist = shortestDistance(cameraPosition, dir);
    vec3 point = cameraPosition + dist * dir;
    vec3 normal = normal(point);
    vec3 domainDir = normalize(abs(cameraPosition - domain(point)));
    vec3 brightnessDir = (point.y < 0.1 && normal.y > 0.9) ? dir : domainDir;
    float brightness = max(0., abs(dot(brightnessDir, normal)));
    //brightness = round(brightness*8.)/8.;
    vec3 color = pow(vec3(pow(brightness, 1.1)*1.2, brightness*0.6, brightness*0.8+0.15), vec3(2.0));
    fragColor = vec4(dist >= MAX_RAY_LEN ? vec3(0) : color, 1.);
}

#ifndef shadertoy
void main() {
    mainImage(fragColor, fragCoord.xy);
}
#endif
