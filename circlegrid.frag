//Set this define for shadertoy
//#define shadertoy

#ifndef shadertoy
#define fragColor gl_FragColor
#define fragCoord gl_FragCoord
#define iResolution u_resolution
#define iMouse u_mouse
#define iTime u_time
#endif

#define PI 3.1415926538

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

mat2 rotate2d(float a) {
    return mat2(
            cos(a), -sin(a),
            sin(a),  cos(a)
            );
}

mat2 scale(float n) {
    return mat2(
            1./n, 0,
            0, 1./n
            );
}

float rand1(vec2 i){
    return fract(sin(dot(i.xy ,vec2(15.736564920423735,87.35522147781187))) * 43758.5453);
}

float rand2(vec2 i){
    return fract(sin(dot(i.xy ,vec2(40.76490260423632,57.86582884666819))) * 43758.5453);
}

float rand3(vec2 i){
    return fract(sin(dot(i.xy ,vec2(14.23444733758451,30.335051567295135))) * 43758.5453);
}

float rand4(vec2 i){
    return fract(sin(dot(i.xy ,vec2(42.30172930474168,11.00981003611844))) * 43758.5453);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float circle(vec2 c, float r) {
    float t = 0.18;
    float circle = (1.-smoothstep(r*0.99, r*1.01, length(c))) * (smoothstep(r*0.89-t, r*1.01-t, length(c)));
    float halo = pow((1.-length(c))*r*2., 2.);
    return clamp(circle + halo, 0., 1.);
}

float radius(vec2 gs, float t) {
    float frequencyMultiplier = rand4(gs) * 1.5 + 0.5;
    return 0.25 + sin(fract(t*frequencyMultiplier + rand4(gs))*2.*PI)*0.10;
}

vec3 fgcolor(vec2 gs, float t) {
    float hue = fract(rand1(gs)*1000.)/4. + 0.7;
    //hue += fract(t/100.)*2.*PI;
    float sat = rand3(gs) * 0.5 + 0.5;
    //return vec3(0);
    return rand4(gs) > 0.2 ? hsv2rgb(vec3(hue,sat,1.)) : vec3(0);
}

float timesin(float t, float multiplier) {
    return sin(fract(t*multiplier) * 2.*PI);
}

vec3 bgcolor(vec2 gs, float t) {
    float hue = fract(rand2(gs)*1000.)/8. + 0.5;
    //hue += fract(t/100.)*2.*PI;
    float lightup = smoothstep(0.25, 0.3, radius(gs,t));
    float biome = pow(timesin(gs.y, 0.005)*0.5+0.5, 3.);
    return hsv2rgb(vec3(
                hue,
                1.-lightup*0.2,
                biome*0.4 + rand3(gs)*0.1 + (lightup*(0.3*biome + 0.1))
                ));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    float time = iTime/2.;
    float pitch = 60.;
    vec2 middle = iResolution.xy/2.;

    float weirdness = timesin(time, 0.1) * 0.;
    float angle = timesin(time, 0.05)/2.;
    vec2 tc = fragCoord.xy; // transformed coordinates
    tc -= middle;
    tc *= rotate2d(angle*mix(1., length(tc/100.)*0.1, weirdness));
    tc.y *= mix(1., sin(tc.x/40.)*5., weirdness);
    tc *= scale(timesin(time, .0123) * 0.5 + 1.);
    tc += middle;
    tc += vec2(0., time*600.);

    vec2 gs = floor(tc/pitch); // grid square
    vec2 gc = mod(tc, pitch); // coordinate within the current grid square
    vec2 nc = gc / pitch;
    fragColor = vec4(
            mix(bgcolor(gs, time), fgcolor(gs, time), circle(nc-vec2(.5), radius(gs, time))),
            1.
            );
}

#ifndef shadertoy
void main() {
    mainImage(fragColor, fragCoord.xy);
}
#endif
