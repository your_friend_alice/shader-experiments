#version 130
//Set this define for shadertoy
//#define shadertoy

#ifndef shadertoy
#define fragColor gl_FragColor
#define fragCoord gl_FragCoord
#define iResolution u_resolution
#define iMouse u_mouse
#define iTime u_time
#endif

#define PI 3.1415926538

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

const int MAX_STEPS = 1000;
const float MIN_RAY_LEN = 0.;
const float MAX_RAY_LEN = 1000.;
const float RAY_HIT_THRESH = 0.01;

const uint k = 1103515245U;  // GLIB C

vec3 hash(uvec3 x) {
    x = ((x>>8U)^x.yzx)*k;
    x = ((x>>8U)^x.yzx)*k;
    x = ((x>>8U)^x.yzx)*k;
    return vec3(x)*(1.0/float(0xffffffffU));
}

float rand1(vec3 i){
    return fract(sin(dot(i,vec3(15.736564920423735,87.35522147781187,78.89253088762932))) * 43758.5453);
}

vec3 rotateAxis(vec3 p, vec3 axis, float angle) {
    return mix(dot(axis, p)*axis, p, cos(angle)) + cross(axis,p)*sin(angle); // ??? idk, it's from blackle
}

float sphere(vec3 samplePoint, float r) {
    return length(samplePoint) - r;
}

float cube(vec3 samplePoint, float r) {
  vec3 q = abs(samplePoint) - r;
  return length(max(q,0.)) + min(max(q.x,max(q.y,q.z)),0.);
}

vec3 repeat(vec3 samplePoint, vec3 pitch) {
    return mod(samplePoint, pitch)-0.5*pitch;
}

float scene(vec3 samplePoint) {
    vec3 localPoint = repeat(samplePoint+1.5, vec3(3.));
    vec3 gridPos = floor((samplePoint+1.5)/3.);
    localPoint = rotateAxis(
        localPoint,
        //normalize(vec3(rand1(vec3(gridPos.x+0.1, gridPos.y, gridPos.z)), rand1(vec3(gridPos.x, gridPos.y+0.1, gridPos.z)), rand1(vec3(gridPos.x, gridPos.y, gridPos.z+0.1)))),
        normalize(hash(uvec3(ivec3(gridPos)))),
        iTime * (rand1(vec3(gridPos.x+0.2, gridPos.y, gridPos.z))-0.5) * 5.
        );
    float size = 1.;
    return max(sphere(localPoint, 1.*size), cube(localPoint, 0.8*size));
}

vec3 rayDirection(vec2 fragCoord, vec2 image_size, float fov) {
    vec2 xy = fragCoord.xy - image_size.xy / 2.;
    float z = min(image_size.x, image_size.y) / tan(radians(fov) / 2.); // we don't need length(xy) here because fov just refers to the fov of a single axis
    return normalize(vec3(xy, -z));
}

float shortestDistance(vec3 cameraPosition, vec3 rayDirection) {
    float len = MIN_RAY_LEN;
    for (int i=0; i < MAX_STEPS; i++) {
        float dist = scene(cameraPosition + len * rayDirection);
        // isn't there a better way than just getting infinitesimally closer till we're within some tiny radius?
        // if we figured out the normal when we get close to the final surface, couldn't we extrapolate an expected point of intersection that way? then jump up to that, but allow the loop to continue in case we're just grazing past
        if (dist < RAY_HIT_THRESH) {
            return len + dist;
        }
        len += dist;
        if (len >= MAX_RAY_LEN) {
            return MAX_RAY_LEN;
        }
    }
    return MAX_RAY_LEN;
}

vec3 normal(vec3 p) {
    float original_dist = scene(p);
    // we lose a little precision by not having our normal centered on the target point but rather sticking out a bit by RAY_HIT_THRESH, but we're saving 2 SDF evaluations
    return normalize(vec3(
        scene(vec3(p.x + RAY_HIT_THRESH, p.y, p.z)) - original_dist,
        scene(vec3(p.x, p.y + RAY_HIT_THRESH, p.z)) - original_dist,
        scene(vec3(p.x, p.y, p.z + RAY_HIT_THRESH)) - original_dist
    ));
}

vec2 circularMotion(float time, float radius) {
    return vec2(sin(time), cos(time))*radius;
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    float time = iTime/10.;
    vec3 cameraPosition = vec3(circularMotion(time*3., 1.5), time*10.);
    cameraPosition = vec3(1.5);
    vec3 dir = rotateAxis(rayDirection(fragCoord, iResolution.xy, 110.), vec3(0., 1., 0.), time);
    dir = rayDirection(fragCoord, iResolution.xy, 110.);
    float dist = shortestDistance(cameraPosition, dir);
    vec3 point = cameraPosition + dist * dir;
    vec3 normal = normal(point);
    vec3 color = pow(normal*0.5+0.5, vec3(2.));
    fragColor = vec4(dist >= MAX_RAY_LEN ? vec3(0) : color, 1.);
}

#ifndef shadertoy
void main() {
    mainImage(fragColor, fragCoord.xy);
}
#endif
